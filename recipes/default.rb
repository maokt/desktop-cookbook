package "i3"
package "sakura"

package "flashplugin-installer" do
    action :purge
end

if node.role?(:laptop)
    include_recipe "desktop::laptop"
end

package "chromium-browser"
package "gnumeric"

