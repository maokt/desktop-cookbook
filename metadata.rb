name             'desktop'
version          '0.1.0'
description      'Configures my desktop machines'
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
license          'Apache 2.0'
maintainer       'Marty Pauley'
maintainer_email 'marty+chef@martian.org'
supports         'ubuntu'
